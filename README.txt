
-- SUMMARY --

This module adds one more button to shopping cart called "One click buy".
It adds item to the shopping cart and immediately redirects to "/checkout" 
to quickly make an order.

-- REQUIREMENTS --

Commerce module (https://drupal.org/project/commerce).

-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

No configuration is required.
If you want to change button's look and feel, use CSS.
